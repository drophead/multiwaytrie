//============================================================================
// Name        : MultiwayTrie.cpp
// Author      : DropHead
//============================================================================

#include <iostream>
#include "MultiwayTree.h"

using namespace std;

int main() {
	MultiwayTrie MT;
	MT.insert("car");
	MT.insert("men");
	MT.insert("women");
	MT.insert("MultiwayTree");
	MT.insert("insert words");
	MT.remove("car");
	MT.remove("insert words");
	
	return 0;
}
